---
title: "Qui suis-je?"
weight: 1
header_menu: true
---

![Accompagnement à la naissance](/images/doula/claire2.jpg)
*Susana Cruz Photographe*

Je m'appelle Claire, je travaille dans le domaine de la perinatalité depuis 2014 et j'accompagne les familles avec passion et dévouement dans leurs parcours de grossesse, pendant la naissance puis en postnatal.

Maman 4 fois de petits franco-mexicains, c'est au mexique que j'ai été formée. C'est là-bas que j'ai débuté mes accompagnements, tout d'abord en tant qu'éducatrice périnatal, puis birthdoula, conseillère en lactation, peu à peu j'ai entrepris diverses formations, je continue de m'informer et d'apprendre continuellement afin d'offrir des accompagnements le plus personnalisé possible (yoga prénatal, massage, sophrologie...)

![Accompagnement à la naissance](/images/acc_naissance.jpeg)

Ayant à coeur d'accompagner les familles dans le respect de leurs parcours de vie, de leurs envies, de leurs besoins..., mon accompagnement est personnalisé, respectueux et non jugeant.

J'ai toujours travaillé entourée et en réseaux avec divers professionels et équipes pluridisciplinaires afin d'affiner et d'enrichier mes services.

Riche de mon expérience personnelle de mère depuis 13 ans, et de mon expérience professionnelle de 9 ans, ce sont des centaines de familles qui m'ont fait confiance, et plus d'une centaine de naissances que j'ai eu l'honneur d'accompagner. Des naissances qui ont eu lieu à domicile, en structure hospitalière, en maison de naissance.  
Des naissances physiologiques, des naissances avec analgésie péridurale, des naissances dans l'eau et des naissances par césarienne.
Je continue de soutenir les familles en offrant un soutient inconditionnel, une écoute attentive et une présence rassurante.


