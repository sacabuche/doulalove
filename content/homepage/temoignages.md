---
title: "Temoignages"
weight: 4
---

> Je ne peux que recommander de faire appel à Claire lors d’une grossesse ! Après sa venue mon partenaire était en confiance pour la venue de notre bébé et savait comment m’accompagner au mieux. Claire est d’une douceur et bienveillance inégalable. Encore un grand merci à toi ❤️.

[Marie B.](https://goo.gl/maps/NH2hUcvupAGyLR269).

-----
> J'ai pu bénéficier des soins et de la douceur de Claire lors d'un massage pré-natal puis d'un soin Rebozo. Claire est discrète, très attentionnée, à l'écoute et douce. Le soin Rebozo a été une expérience incroyable, un moment entre femmes que j'ai pu partager avec une amie d'enfance. Un moment où un cercle de femme se réuni pour échanger autour de la féminité, de la maternité. Le chocolat de début de cérémonie était divin, j'en garde un excellent souvenir. Merci Claire pour ces moments!

[Chloé M.](https://goo.gl/maps/HGA2VxAh7ryTC5Bq9)

------

> Je recommande Claire à 100%, je ne la connaissais que peu le jour de mon accouchement et elle a su se positionner correctement. Elle a été très présente aux moments où j'en ressentais le besoin, me proposant diverse technique pour me soulager. Une vrai perle!

[Melusine N.](https://goo.gl/maps/3EPYEJzUax3TZRjt9)

-----

> Claire m’a été d’une aide précieuse pour accueillir mon bébé !! Et quelle joie de la revoir pour un soin rebozzo ... elle a des mains d’or qui font un bien fou aussi bien au corps qu’au cœur !!! Merci merciii

[Marie-Liesse G.](https://goo.gl/maps/2rhLE9xvE4EPo9mA7)

------

> Claire m’a accompagné lors de ma préparation à l’accouchement, durant mon accouchement et a été présente lors de mes interrogations quant à l’allaitement de mon enfant.  
Je n’ai pas d’autres mots que présence, bienveillance, écoute et douceur qui me viennent à l’esprit.  
Pendant ma préparation Claire a très rapidement su me conseiller, m’accompagner et me rassurer.  
Durant mon accouchement Claire a su autant être présente que discrète avec une douceur et une bienveillance qui me marqueront.  
Après la naissance de ma fille, elle a répondu et accompagné lors de mes questionnement et étapes de l’allaitement.  
C’est alors avec une grande confiance que j’ai fait de nouveau appel à Claire pour la prestation massage, bain et soin Rebozo.  
J’ai été transportée par la douceur, la technicité et la bienveillance de Claire.  
Un soin bien au delà de mes attentes.  
Je suis ressortie légère, reposée et sereine de cette prestation.  
Un grand merci à Claire et je recommande à 100%.  

[Pauline D.](https://goo.gl/maps/pipCaFZzkN61D1K8A)