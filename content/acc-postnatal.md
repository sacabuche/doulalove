---
title: "Postnatal"
---

---

#### ACCOMPAGNEMENT POSTNATAL
![Massage prénatal](/images/acc_postnatal.jpeg)
*Charlène Ragues Photographe*

---

#### MASSAGE POSTNATAL

Le massage postnatal est un massage à l'huile biologique, choisi spécifiquement pour les jeunes mamans.  
Un moment pour prendre soin du corps qui vient de passer par l'épreuve physique et émotionnelle de la naissance, qu'elle soit voie haute ou voie basse.  
Un massage tout en douceur pour cocooner la femme, qui soulage les douleurs musculaires, douleurs dorsales et tensions qui sont fréquentes en post-partum.  
Je masse le corps entier, de la tête au pieds, afin de favoriser un lâcher-prise, la détente et la relaxation du corps et du mental.  
Prendre un moment pour un massage en période de post-partum, est une manière de retrouver un temps pour soi, pour remplir de réservoir d'énergie, qui est souvent mis à mal à cause du manque de sommeil, du stress, de la chute hormonale... C'est une manière revitalisante de prendre soin des jeunes mamans, qui pourront ainsi se sentir plus reposées, et qui pourront à leur tour prendre soin de leurs nouveau-nés.

![Massage prénatal](/images/doula/massage1.jpg)
*Susana Cruz Photographe*

---

![Massage prénatal](/images/doula/massage.jpg)
*Susana Cruz Photographe*

---

![Massage prénatal](/images/doula/massage4.jpg)
*Susana Cruz Photographe*

---

#### LA CERRADA -  MASSAGE AVEC REBOZO 

> Massage Rebozo - Resserrage du bassin 

IL sagit d'un massage réalisé avec des rebozos (châles mexicains).  
Un moment cocon de douceur , où j'utilise 7 rebozos qui deviennent les extensions de mes mains.  
Pendant 1h30, ce soin sera composé d'étirements, de bercements, d'enveloppements et de resserages du corps, des pieds à la tête.  
Particulièrement bénefique en post partum, vous vous sentirez contenue et maternée, cela permet une detente et un profond relachement des muscles.  

Le resserage du bassin et du corps, souvent ressenti comme ouvert, eparpillé après la naissance, vient soutenir et redéfinir les contours, cela permet de se recentrer et se retrouver, soulage les douleurs du post partum, élimine les tensions, la fatigue et le stress.  
La douceur et la lenteur de ce moment, sont également l'occasion de prendre le temps de lacher prise et d'integrer symboliquement la fermeture d'une étape.  

Ce massage peut être réalisé dés la naissance, quelques semaines, mois, même années après. Il est également bénéfique pour toutes les personnes, qu'elles aient vécues ou non un enfantement.

![Rebozo, cerrada](/images/doula/cerrada.jpeg)

---

![Rebozo, cerrada](/images/doula/rebozo4.jpg)
*Susana Cruz Photographe*

---

#### RITUEL DE FERMETURE - REBOZO

> Massage et Cerrada rebozo - Resserrage du bassin 

Le rituel de fermeture, est un momen d'intense relaxation et de profonde détente.
Une parenthèse pour prendre soin du corps et de l'esprit.  

Je commence par un massage à l'huile, un massage intuitif qui soulage les tensions et douleurs fréquemment présente après une naissance.  

Un massage tout en douceur et en lenteur, qui est complété par un massage avec les rebozos.  
Étirements, bercements, relachements... grâce aux rebozos nous continuons d'approfondir la détente. Le serrage du corps de la tête aux pieds, vient compléter ce soin et prendre le relais de vos muscles, qui vont pouvoir se détendre en profondeur.

Le serrage du bassin qui à été très solicité par la grossesse et la naissance, est particulièrement appréciable pour les femmes, il est contenu et rassemblé, ce qui permet aux tendons, aux os, aux articulations, aux ligaments... de se repositionner, de se refermer.

![Rebozo, cerrada](/images/doula/rebozo2.jpg)
*Susana Cruz Photographe*

---

#### RITUEL POSTNATAL - REBOZO 

> Massage, bains aux plantes et Cerrada Rebozo - Resserrage du bassin

Le Rituel post PARTUM, commence par un temps de parole. Nous ouvrons la séance en partageant une tisane, et la femme prend le temps de confier son récit d'accouchement si elle le souhaite.

Dans un second temps, on s'installe pour un massage relaxant à l'huile, un massage intuitif tout en douceur pour prendre soin du corps et relâcher les éventuelles tensions et douleurs accumulés suite à la grossesse et la naissance.

Ensuite vient le temps du bain aux plantes. Elles sont sélectionnées en adéquation avec les besoins de la femme, infusées en amont, puis versées dans un bain chaud dans une ambiance tamisée et cocooning.

A la sortie du bain, vient le temps de la cerrada avec les rebozos, le massage et bercements, puis le serrage du corps de la tête aux pieds pour une détente approfondie. Le corps est contenu et rassemblé pour fermer physiquement et symboliquement cette étape de vie.


Durée aproximative: 3h30

![Rituel post partum](/images/doula/rituel3.jpg)
*Susana Cruz Photographe*

----

#### ALLAITEMENT

![Alleitement bebe](/images/allaitement2.jpeg)
*Charlène Ragues Photographe*

---

#### BAIN ENVELOPPÉ BÉBÉ

La bain enveloppé offre une approche douce et apaisante pour le bain de votre tout-petit. Elle favorise le bien-être émotionnel et physique de votre bébé, tout en créant un moment privilégié de connexion parent-enfant.
Cette pratique consiste à enmailloter bébé dans un lange doux avant de le plonger délicatement dans l'eau chaude du bain. Enveloppé ainsi, il se rappel des sensations qu'il à connue in utéro.

![Bain enveloppé bébé endormi](/images/bain-enveloppe.jpeg)

---

![Bain enveloppé bébé](/images/doula/bain.jpg)

