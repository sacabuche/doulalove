---
title: ""
---

|  |  |  |
| :-- | :---: | --: |
| [Accompagnement postnatal](/acc-postnatal#accompagnement-postnatal) | 1h30 - 2h | 60€ |
| [Massage postnatal](/acc-postnatal#massage-postnatal) | 1h30 | 70€ |
| [Massage prenatal](/acc-prenatal#massage-prénatal) | 1h30 | 70€ |
| [Cerrada (rebozo)](/acc-postnatal#la-cerrada----massage-avec-rebozo) | 1h30 | 70€ |
| [Rituel de fermeture](/acc-postnatal#rituel-de-fermeture---rebozo) | 2h30 | 140€ |
| [Rituel postnatal](/acc-postnatal#rituel-postnatal---rebozo) | 3h30 | 180€ |
| [Bain Enveloppé](/acc-postnatal#bain-enveloppé-bébé) | 1h30 | 70€ |