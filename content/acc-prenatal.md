---
title: "Prénatal"
---

---

#### ACCOMPAGNEMENT PRÉNATAL [^note]

Je vous accompagne pendant la grossesse en rendez-vous individuel ou en couple, à n'importe quel stade de la grossesse.

Je vous offre un espace d'écoute bienveillant, et nous échangeons sur les sujets qui sont pertinents pour vous au moment présent. C'est votre espace, entièrement flexible et adapté à vos envies et vos besoins. Un espace pour y déposer vos peurs, doutes, interrogations... Je peux pendant nos rencontres, vous apporter les informations dont vous avez besoin en fonction de vos demandes, pour vous aider à faire vos choix et prendre des décisions éclairées.

Lors de nos recontres d'une durée d'1h30 à 2h, nous cheminerons ensemble autour de divers sujets tel que votre projet de naissance, pendant la grossesse, techniques pour gérer la douleur des contractions, le rôle de l'accompagnant pendant la naissance, l'allaitement, organiser le post-partum...

Ce soutient émotionnel et pratique complètement personnalisé, à pour but de vous permettre de vivre cette expérience de manière sereine et positive.

---

#### MASSAGE PRÉNATAL

Il s'agit d'un massage relaxant réalisé sur table avec de l'huile de sésame biologique.

Un moment suspendu de pure détente, pendant lequel vous pourrez relacher les tensions dues a la grossesse.

Un instant de bien être qui permet de connecter avec ce corps qui change tellement vite, et de souager le stress, la fatigue, les tensions musculaires et articulaires, les crampes, la fatigue... grâce aux effleurages, modelages, pétrissages, pressions très douces.

Ce massage tout en douceur est le moment idéal pour apaiser le corps et le mental, vivre une parenthèse de connexion avec bébé et favoriser la sécrétion d'ocytocine, l'hormone du plaisir que bébé va ressentir.

![Massage prénatal](/images/doula/massage_prenatal.jpeg)

---

![Massage prénatal](/images/doula/massage_prenatal2.jpeg)

---

![Massage prénatal](/images/doula/massage_prenatal4.jpeg)

---


#### REBOZO EN PRÉNATAL

En prénatal, je manie les rebozos pour masser et détendre la femme enceinte.  
C'est un massage qui se pratique habillé, sans contact physique direct, dans des positions adaptées à la grossesse et au confort de la femme enceinte.  
Ce soin permet une détente globale du corps et de l'esprit. Le corps est massé, bercé, porté par le rebozo qui est un prolongement de mes mains, il permet de mobiliser le corps tout en douceur.  
Celà favorise la connexion avec bébé qui ressent les bienfaits et les bercements tout comme sa maman.

![Rebozo, en prénatal](/images/doula/rebozo1.jpg)
*Susana Cruz Photographe*


[^note]: *L'accompagnement périnatal proposé est entièrement non-médical et non-thérapeutique. Il ne remplace en aucun cas le suivi médical réalisé par un professionnel de santé qualifié pendant la grossesse, lors de la naissance et en période post-natal.  
 Une doula n'est pas une sage-femme et n'est en aucun cas habilitée à établir des diagnostics, à réaliser des actes médicaux relatifs à la santé de la mère et du bébé, au bon déroulement de la grossesse, de l'accouchement ou des suites de couches.*