---
title: "Massage prénatal"
---

![Massage prénatal](/images/doula/massage_prenatal7.jpeg)


Il s'agit d'un massage relaxant réalisé sur table avec de l'huile de sésame biologique.

Un moment suspendu de pure détente, pendant lequel vous pourrez relacher les tensions dues a la grossesse.

Un instant de bien être qui permet de connecter avec ce corps qui change tellement vite, et de souager le stress, la fatigue, les tensions musculaires et articulaires, les crampes, la fatigue... grâce aux effleurages, modelages, pétrissages, pressions très douces.

Ce massage tout en douceur est le moment idéal pour apaiser le corps et le mental, vivre une parenthèse de connexion avec bébé et favoriser la sécrétion d'ocytocine, l'hormone du plaisir que bébé va ressentir.

![Massage prénatal](/images/doula/massage_prenatal.jpeg)

---

![Massage prénatal](/images/doula/massage_prenatal2.jpeg)

---

![Massage prénatal](/images/doula/massage_prenatal4.jpeg)

---

![Massage prénatal](/images/doula/massage_prenatal5.jpeg)

---

![Massage prénatal](/images/doula/massage_prenatal6.jpeg)


